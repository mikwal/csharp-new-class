# Visual Studio Code extension `csharp-new-file`

## Features

* Add new C# class, interface, enum, record or struct from file explorer menu

## Extension Settings

_None at the moment._

## Release Notes

### 0.1.0

Initial release

### 0.2.0

Add `interface` and `enum` file templates

### 0.3.0

Resolve root namespace from `<RootNamespace>` or `<AssemblyName>` project file property

### 0.4.0

Add `record` and `struct` file templates and remove `namespace` indentation.
